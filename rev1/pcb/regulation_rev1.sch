EESchema Schematic File Version 4
LIBS:regulation_rev1-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4000 1300 1650 1350
U 5BAE459F
F0 "regulator5V" 50
F1 "regulator.sch" 50
F2 "EN" I L 4000 1850 50 
F3 "PGOOD" I L 4000 2050 50 
F4 "VIN" I L 4000 1650 50 
F5 "VOUT" O R 5650 1850 50 
F6 "GND" O R 5650 2050 50 
$EndSheet
$Comp
L Device:Battery BT1
U 1 1 5BAEFC35
P 2250 1900
F 0 "BT1" H 2358 1946 50  0000 L CNN
F 1 "Battery" H 2358 1855 50  0000 L CNN
F 2 "battery_holder:BatteryHolder_Keystone_1048P" V 2250 1960 50  0001 C CNN
F 3 "~" V 2250 1960 50  0001 C CNN
	1    2250 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 1700 2250 1650
Wire Wire Line
	2250 1650 2600 1650
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5BAF2D75
P 3000 1950
F 0 "J1" H 3106 2128 50  0000 C CNN
F 1 "EN setting" H 3106 2037 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3000 1950 50  0001 C CNN
F 3 "~" H 3000 1950 50  0001 C CNN
	1    3000 1950
	1    0    0    -1  
$EndComp
Text Notes 3700 2200 2    50   ~ 0
Tie headers together to disable IC
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J2
U 1 1 5BAF33FB
P 3050 3900
F 0 "J2" H 2800 4450 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 3300 4450 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x10_Pitch2.54mm" H 3050 3900 50  0001 C CNN
F 3 "~" H 3050 3900 50  0001 C CNN
	1    3050 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_Small D1
U 1 1 5BAF3644
P 4900 3650
F 0 "D1" H 4900 3445 50  0000 C CNN
F 1 "PGOOD" H 4900 3536 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" V 4900 3650 50  0001 C CNN
F 3 "~" V 4900 3650 50  0001 C CNN
	1    4900 3650
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_Small D2
U 1 1 5BAF36F4
P 4900 4000
F 0 "D2" H 4900 3795 50  0000 C CNN
F 1 "VOUT" H 4900 3886 50  0000 C CNN
F 2 "LEDs:LED_0805_HandSoldering" V 4900 4000 50  0001 C CNN
F 3 "~" V 4900 4000 50  0001 C CNN
	1    4900 4000
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small R1
U 1 1 5BAF37F5
P 5300 3650
F 0 "R1" V 5496 3650 50  0000 C CNN
F 1 "1k" V 5405 3650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5300 3650 50  0001 C CNN
F 3 "~" H 5300 3650 50  0001 C CNN
	1    5300 3650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R2
U 1 1 5BAF3850
P 5300 4000
F 0 "R2" V 5496 4000 50  0000 C CNN
F 1 "1k" V 5405 4000 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 5300 4000 50  0001 C CNN
F 3 "~" H 5300 4000 50  0001 C CNN
	1    5300 4000
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5BAF3C01
P 6800 1900
F 0 "J3" H 6880 1892 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 6880 1801 50  0000 L CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_bornier-2_P5.08mm" H 6800 1900 50  0001 C CNN
F 3 "~" H 6800 1900 50  0001 C CNN
	1    6800 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 1850 3350 1850
Wire Wire Line
	3350 1850 3350 1950
Wire Wire Line
	3350 1950 3200 1950
$Comp
L power:GNDPWR #PWR0101
U 1 1 5BAF3FDA
P 3300 2150
F 0 "#PWR0101" H 3300 1950 50  0001 C CNN
F 1 "GNDPWR" H 3304 1996 50  0000 C CNN
F 2 "" H 3300 2100 50  0001 C CNN
F 3 "" H 3300 2100 50  0001 C CNN
	1    3300 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0102
U 1 1 5BAF404E
P 2250 2250
F 0 "#PWR0102" H 2250 2050 50  0001 C CNN
F 1 "GNDPWR" H 2254 2096 50  0000 C CNN
F 2 "" H 2250 2200 50  0001 C CNN
F 3 "" H 2250 2200 50  0001 C CNN
	1    2250 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0103
U 1 1 5BAF40A1
P 3550 3450
F 0 "#PWR0103" H 3550 3250 50  0001 C CNN
F 1 "GNDPWR" H 3554 3296 50  0000 C CNN
F 2 "" H 3550 3400 50  0001 C CNN
F 3 "" H 3550 3400 50  0001 C CNN
	1    3550 3450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR0104
U 1 1 5BAF40BC
P 5750 4200
F 0 "#PWR0104" H 5750 4000 50  0001 C CNN
F 1 "GNDPWR" H 5754 4046 50  0000 C CNN
F 2 "" H 5750 4150 50  0001 C CNN
F 3 "" H 5750 4150 50  0001 C CNN
	1    5750 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1850 5800 1850
Wire Wire Line
	6600 1850 6600 1900
Wire Wire Line
	6600 2000 6600 2050
Wire Wire Line
	6600 2050 5650 2050
Wire Wire Line
	5400 4000 5750 4000
Wire Wire Line
	5750 4000 5750 4200
Wire Wire Line
	5400 3650 5750 3650
Wire Wire Line
	5750 3650 5750 4000
Connection ~ 5750 4000
Wire Wire Line
	5200 4000 5000 4000
Wire Wire Line
	5000 3650 5200 3650
Wire Wire Line
	4000 2050 3800 2050
Wire Wire Line
	3800 2050 3800 3650
Wire Wire Line
	3800 3650 4800 3650
Wire Wire Line
	2850 3500 2500 3500
Wire Wire Line
	2500 3500 2500 3600
Wire Wire Line
	2850 3600 2500 3600
Wire Wire Line
	3350 3600 3350 3500
Wire Wire Line
	3350 3400 2500 3400
Wire Wire Line
	2500 3400 2500 3500
Connection ~ 3350 3500
Wire Wire Line
	3350 3500 3350 3400
Connection ~ 2500 3500
Wire Wire Line
	4800 4000 4100 4000
Wire Wire Line
	4100 4000 4100 2850
Wire Wire Line
	4100 2850 5800 2850
Wire Wire Line
	5800 2850 5800 1850
Connection ~ 5800 1850
Wire Wire Line
	5800 1850 6600 1850
Wire Wire Line
	3200 2050 3300 2050
Wire Wire Line
	3300 2050 3300 2150
Wire Wire Line
	2250 2100 2250 2250
Wire Wire Line
	3550 3450 3550 3400
Wire Wire Line
	3550 3400 3350 3400
Connection ~ 3350 3400
Text Label 6050 1850 2    50   ~ 0
VOUT
$Comp
L power:GNDPWR #PWR0105
U 1 1 5BAF6D19
P 6600 2150
F 0 "#PWR0105" H 6600 1950 50  0001 C CNN
F 1 "GNDPWR" H 6604 1996 50  0000 C CNN
F 2 "" H 6600 2100 50  0001 C CNN
F 3 "" H 6600 2100 50  0001 C CNN
	1    6600 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 2050 6600 2150
Connection ~ 6600 2050
Wire Wire Line
	2850 3700 2850 3800
Wire Wire Line
	2850 3700 2700 3700
Connection ~ 2850 3700
Wire Wire Line
	3350 3700 3500 3700
Wire Wire Line
	3350 3700 3350 3800
Connection ~ 3350 3700
Text Label 3500 3700 0    50   ~ 0
VOUT
Text Label 2700 3700 2    50   ~ 0
VOUT
Wire Wire Line
	3350 3900 3350 4000
Connection ~ 3350 4000
Wire Wire Line
	3350 4000 3350 4100
Connection ~ 3350 4100
Wire Wire Line
	3350 4100 3350 4200
Wire Wire Line
	2850 4200 2850 4100
Connection ~ 2850 4000
Wire Wire Line
	2850 4000 2850 3900
Connection ~ 2850 4100
Wire Wire Line
	2850 4100 2850 4000
Text Label 2550 1650 2    50   ~ 0
VBAT
Wire Wire Line
	2850 4300 2850 4400
Wire Wire Line
	2850 4550 3350 4550
Wire Wire Line
	3350 4550 3350 4400
Connection ~ 2850 4400
Wire Wire Line
	2850 4400 2850 4550
Connection ~ 3350 4400
Wire Wire Line
	3350 4400 3350 4300
$Comp
L power:GNDPWR #PWR0106
U 1 1 5BAFD73A
P 3350 4600
F 0 "#PWR0106" H 3350 4400 50  0001 C CNN
F 1 "GNDPWR" H 3354 4446 50  0000 C CNN
F 2 "" H 3350 4550 50  0001 C CNN
F 3 "" H 3350 4550 50  0001 C CNN
	1    3350 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 4600 3350 4550
Connection ~ 3350 4550
$Comp
L Device:R_Shunt R9
U 1 1 5BAEE617
P 2800 1650
F 0 "R9" V 2668 1650 50  0000 C CNN
F 1 "R_Shunt" V 2577 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_Shunt_Ohmite_LVK12" V 2730 1650 50  0001 C CNN
F 3 "~" H 2800 1650 50  0001 C CNN
	1    2800 1650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3000 1650 3250 1650
Wire Wire Line
	3700 4000 3700 2850
Wire Wire Line
	3700 2850 1650 2850
Wire Wire Line
	1650 2850 1650 1250
Wire Wire Line
	1650 1250 3250 1250
Wire Wire Line
	3250 1250 3250 1650
Wire Wire Line
	3350 4000 3700 4000
Connection ~ 3250 1650
Wire Wire Line
	3250 1650 4000 1650
Wire Wire Line
	1650 4000 1650 2850
Wire Wire Line
	1650 4000 2850 4000
Connection ~ 1650 2850
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5BAF57B6
P 2700 1400
F 0 "H1" H 2800 1451 50  0000 L CNN
F 1 "MountingHole_Pad" H 2800 1360 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 2700 1400 50  0001 C CNN
F 3 "~" H 2700 1400 50  0001 C CNN
	1    2700 1400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5BAF7C0E
P 2900 1400
F 0 "H2" H 3000 1451 50  0000 L CNN
F 1 "MountingHole_Pad" H 3000 1360 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2_Pad" H 2900 1400 50  0001 C CNN
F 3 "~" H 2900 1400 50  0001 C CNN
	1    2900 1400
	1    0    0    -1  
$EndComp
$EndSCHEMATC
